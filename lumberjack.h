#ifndef LIBLOG_H
#define LIBLOG_H

#include <Print.h>

// uncomment or define this to strip debugging from the codebase, reducing
// overall size and speeding it up.
#define LOG_MAX_ZION

namespace LumberJack {

/*
 * Type Definitions
 */
enum LogLevel : uint8_t {
    QUIET       = 0,
    FATAL       = 1,
    ERROR       = 2,
    WARN        = 3,
    INFO        = 4,
    DEBUG       = 5,
    OVERWHELM   = 6,
    ZION        = 7
};

typedef uint8_t LogContext;
typedef uint8_t LogOutput;
typedef uint8_t mask8_t;

// Output Function
typedef bool (*logOutputFunc)(
        const String &level,
        const String &Context,
        const String &);


/* Helper Functions
 */
// converts a mask to an integer or -1 for failure.
int bitPos( const uint8_t &mask );

// finds the location of the first 0 bit in a mask
int firstEmptyBitNum( const uint8_t &mask );

// gives the bitmask of the the first available bit.
uint8_t firstEmptyBit( const uint8_t &mask );

/*
 * logger singleton
 */
class Logger {
    /* Singleton boilerplate */
private:
    Logger() {}
    ~Logger() {}

public:
    static Logger& get(){
        static Logger instance;
        return instance;
    }
    Logger( Logger const& )         = delete;
    void operator=( Logger const& ) = delete;

private:
    //output struct
    struct Output {
        String name = "";
        uint8_t bit = 0;
        logOutputFunc func = nullptr;
    };

    //output
    mask8_t     output_slots    = 0x00;
    mask8_t     output_mask     = 0xFF;
    Output      outputs[ 8 ];

    //context
    mask8_t     context_slots   = 0x01;
    mask8_t     context_mask    = 0xFF;
    LogContext  context         = 0b00000000;
    String      contexts[ 8 ]{"DEFAULT","","","","","","",""};

    //level
    LogLevel    level           = LogLevel::INFO;
    LogLevel    level_default   = LogLevel::INFO;
    const String levels[ 8 ]{
        "QUIET",
        "FATAL",
        "ERROR",
        "WARN",
        "INFO",
        "DEBUG",
        "OVERWHELM",
        "ZION"
    };

public:
    LogOutput   registerOutput( const String &name, logOutputFunc func );
    void        removeOutput( LogOutput &output );

    LogContext  registerContext( const String &key );
    void        removeContext( const LogContext &context );

    void        setOutputMask( const mask8_t &mask );
    mask8_t     getOutputMask() { return output_mask; }

    void        setContextMask( const mask8_t &mask );
    mask8_t     getContextMask() { return context_mask; }

    bool        setContext( const LogContext &context );
    LogContext  getContext() { return context; }

    void        setLevel( const LogLevel &level );
    LogLevel    getLevel() { return level; }

    void        setLevelDefault( const LogLevel &level );
    LogLevel    getLevelDefault() { return level_default; }

    String      contextStr( const LogContext &context );
    String      levelStr( const LogLevel &level );

    // Logger Variadic function
    // ========================
    template<typename ... Args>
    void log( const LogLevel &ll, const LogContext &lc, Args ... args){
        if( lc & ~context_mask ) return;
        if( ll > level ) return;

        for( int i = 0; i < 8 && (1 << i) & output_slots & output_mask; ++i ){
            //DISABLE the loggers whilst logging to prevent poorly implemented
            // logging functions.
            mask8_t hold = output_mask;
            output_mask = 0x00;
            bool result = outputs[i].func( levelStr(ll), contextStr(lc), buildMessage( args ... ) );
            output_mask = hold;
            //re-enable logging
            if(! result ){
                //mask this logger and log the  error
                output_mask &= ~(1 << i);
                log( LogLevel::ERROR, context, F("Failed to log using"), outputs[i].name, F("disabling.") );
            }
        }
    }
    // Variadic function template for building messages to
    // hand to the logger using recusive expansion.
    // TODO change these variadic templates to fold expressions when possible
    String buildMessage() { return ""; }//end of chain

    // default template
    template<typename T, typename ... Args>
    String buildMessage( T first, Args ... args ){
        return String( first ) + " " + buildMessage( args ... );
    }
};

namespace LOGGERS {
    using LumberJack::LogLevel;

/*
 * LumberJack::LOG templates for easy logging
 * ========================================== */
template<typename ... Args>
void LOG( Args ... args ){
    Logger::get().log( Logger::get().getLevel(), Logger::get().getContext(), args ... );
}

template<typename ... Args>
void LOG( const LogLevel &ll, Args ... args ){
    Logger::get().log( ll, Logger::get().getContext(), args ... );
}

template<typename ... Args>
void LOG( const LogLevel &ll, const LogContext &lc, Args ... args ){
    Logger::get().log( ll, lc, args ... );
}

//Tiered macros such that the highest value enables all the lower levels.
#define LOGZ( ... )
#define LOGO( ... )
#define LOGD( ... )
#define LOGI( ... )
#define LOGW( ... )
#define LOGE( ... )
#define LOGF( ... )

#ifdef LOG_MAX_ZION
#define LOG_MAX_OVERWHELM
#undef LOGZ
template<typename ... Args>
void LOGZ( Args ... args ){
    LOG( LogLevel::ZION, args ... );
}
#endif

#ifdef LOG_MAX_OVERWHELM
#define LOG_MAX_DEBUG
#undef LOGO
template<typename ... Args>
void LOGO( Args ... args ){
    LOG( LogLevel::OVERWHELM, args ... );
}
#endif

#ifdef LOG_MAX_DEBUG
#define LOG_MAX_INFO
#undef LOGD
template<typename ... Args>
void LOGD( Args ... args ){
    LOG( LogLevel::DEBUG, args ... );
}
#endif

#ifdef LOG_MAX_INFO
#define LOG_MAX_WARN
#undef LOGI
template<typename ... Args>
void LOGI( Args ... args ){
    Logger::get().log( LogLevel::INFO, Logger::get().getContext(), args ... );
}
template<typename ... Args>
void LOGI( const LogContext &context, Args ... args ){
    Logger::get().log( LogLevel::INFO, context, args ... );
}
#endif

#ifdef LOG_MAX_WARN
#define LOG_MAX_ERROR
#undef LOGW
template<typename ... Args>
void LOGW( Args ... args ){
    LOG( LogLevel::WARN, args ... );
}
#endif

#ifdef LOG_MAX_ERROR
#define LOG_MAX_FATAL
#undef LOGE
template<typename ... Args>
void LOGE( Args ... args ){
    LOG( LogLevel::ERROR, args ... );
}
#endif

#ifdef LOG_MAX_FATAL
#undef LOGF
template<typename ... Args>
void LOGF( Args ... args ){
    LOG( LogLevel::FATAL, args ... );
}
#endif

} //end namespace LOGGERS
} //end namespace Lumberjack

#endif //LIBLOG_H
