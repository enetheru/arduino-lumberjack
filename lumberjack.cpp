#include "lumberjack.h"
namespace LumberJack {
    using namespace LOGGERS;

// converts a mask to an integer or -1 for failure.
int bitPos( const uint8_t &mask ){
    uint8_t i = 0;
    for( uint8_t j = 0b00000001; j < 128; j <<= 1 ){
        if( j & mask ) return i;
        ++i;
    }
    return -1;
}

// returns the position of the first available bit or -1
int firstEmptyBitNum( const uint8_t &mask ){
    for( int i = 0; i < 8; ++i ){
        int test = 1 << i;
        if( test & ~mask) return i;
    }
    return -1;
}

// returns the bitmask of the first available bit from a mask or 0
uint8_t firstEmptyBit( const uint8_t &mask ){
    for( int bit = 1; bit != 256; bit = bit << 1 ){
        if( bit & ~mask )return bit;
    }
    return 0;
}


/* Log Level
 */
void
Logger::setLevelDefault( const LogLevel &ll ){
    level_default = ll;
    if( level_default > ZION )level_default = QUIET;
}

void
Logger::setLevel( const LogLevel &ll ){
    level = ll;
    if( level > ZION )level = QUIET;
}

String
Logger::levelStr( const LogLevel &level ){
    return levels[ level ];
}

/* Log Output
 */
// Registers new logging function and returns bitmask for it.
LogOutput
Logger::registerOutput( const String &name, logOutputFunc func ){
    uint8_t bitflag = firstEmptyBit( output_slots );
    if(! bitflag ){
        LOGE( F("Unable to register LO"), name, F(", no positions left.") );
        return 0;
    }
    if(! func( levelStr(LogLevel::INFO),
                contextStr(0x01),
                String( F("testing LO function(") ) + name + F(")") ) ){
        LOGE( F("Failed output test, unable to register LO"), name );
        return 0;
    }

    Output &output = outputs[ bitPos( bitflag ) ];
    output.name = name;
    output.bit = bitflag;
    output.func = func;

    output_slots |= bitflag;
    LOGI( F("Registered LogOutput"), name, F("at position:"), String( bitPos( bitflag ) ) );
    return bitflag;
}

void
Logger::removeOutput( LogOutput &output ){
    while( bitPos( output ) >= 0 ){
        LOGI( F("Removing LogOutput"), outputs[ bitPos( output ) ].name,
              F("at position:"), String( bitPos( output ) ) );
        outputs[ bitPos( output ) ].name = "";
        outputs[ bitPos( output ) ].bit = 0;
        outputs[ bitPos( output ) ].func = nullptr;
    }
    output_slots &= ~context;
}

void
Logger::setOutputMask( const mask8_t &mask ){
    output_mask = mask;
    LOGI( F("Setting LogOutput Mask"), String( output_mask, BIN ) );
};

/* Log Context
 */

LogContext
Logger::registerContext( const String &key ){
    uint8_t bitflag = firstEmptyBit( context_slots );
    if(! bitflag ){
        LOGE( F("Unable to register LogContext"), key,
                F(", no positions left.") );
        return 0;
    }
    LOGI( F("Registered LogContext"), key, F("at position:"),
            String( bitPos( bitflag ) ) );

    contexts[ bitPos( bitflag ) ] = key;
    context_slots |= bitflag;
    return bitflag;
}

void
Logger::removeContext( const LogContext &context ){
    while( bitPos( context ) >= 0 ){
        LOGI( F("Removing LogContext"), contexts[ bitPos( context ) ],
              F("at position:"), String( bitPos( context ) ) );
        contexts[ bitPos( context ) ] = "";
    }
    context_slots &= ~context;
}

bool
Logger::setContext( const LogContext &context ){
    if( context & ~context_slots ){
        this->context = 0x00;
        LOGE( F("setContext received invalid logging context:"), context );
        return false;
    }

    this->context = context;
    LOGI( F("setContext("), context, ")=[",contextStr( context ),"]" );
    return true;
}

void
Logger::setContextMask( const mask8_t &mask ){
    context_mask = mask;
    LOGI( F("Setting LogContext Mask"), String( context_mask, BIN ) );
}

String
Logger::contextStr( const LogContext &context ){
    return contexts[ context ];
}

}
