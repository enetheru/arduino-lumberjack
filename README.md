LumberJack
==========
I really just like the name for a logging library :)

The premise for this was that i had a number of places that needed to have logs
sent to it:

* SD Card
* websocket connections
* serial ports

so I wanted a generic framework that I could simply log to, enable or disable
recivers, add time, context, and severity information to.

the basic idea is that you register a logging object and a logging context
then you just send data to the logger and it will push it out to whatever
providers and contexts you have registered.

it allows me to send system messages to the SD card in a syslog type fashion,
and data from the periphery to the websocket connection, or to sdcard logs.

it simplifies the sending of logs anywhere i want them to go.

Using LumberJack
----------------
`using namespace LumberJack::LOGGERS;` adds only the LOG function variants to
the local namespace. `LOG, LOGZ, LOGO, LOGE, LOG`

Then in the source, instantiate the logger singleton, and register a logger
```c++
#include "lumberjack.h"

using namespace LumberJack::LOGGERS;

LumberJack::Logger &logger = LumberJack::Logger::get();

setup(){
    // Begin Serial Interface
    int baud = 9600;
    Serial.begin( baud );

    // Register logging on Serial port
    logger.registerOutput( F("SERIAL"),
        []( const String &level,
            const String &context,
            const String &message ) -> bool {
                Serial.print( logger.levelStr( level ) );
                Serial.print( " - " );
                Serial.print( logger.contextStr( context ) );
                Serial.print( " - " );
                Serial.println( message );
                return true;
        });
}

loop(){
    LOGI("I'm a LumberJack and I'm ok!");
}
```

Log Outputs
-----------
Registering a function pointer allows any kind of log output
I am using it for loggin to Serial, SD, and over a websocket connection.

Log Levels
----------
Each level of the logging apparatus has its own macro

* ZION        - LOGZ( ... )
* OVERWHELM   - LOGO( ... )
* DEBUG       - LOGD( ... )
* INFO        - LOGI( ... )
* WARNING     - LOGW( ... )
* ERROR       - LOGE( ... )
* FATAL       - LOGF( ... )

Log Contexts
------------
There are eight slots for context and are used as a way to filter
what gets logged so that the observer can focus on what they require without
slowing down the rest of the code.

Stripping logging from codebase
-------------------------------
Each of these defines represents a log level, the max will be the limit of
the logging, and the highest max wins.. so if you define both quiet and
zion, then zion will win.

* LOG_MAX_ZION
* LOG_MAX_OVERWHELM
* LOG_MAX_DEBUG
* LOG_MAX_INFO
* LOG_MAX_WARN
* LOG_MAX_ERROR
* LOG_MAX_FATAL
* LOG_MAX_QUIET

List of things to do
--------------------
* TODO Define global formatter to create the output string.
* TODO LOG_MAX sourc stripping  doesnt work properly at the moment because it
 doesnt strip the source on expansion, rather it defines the LOG[ZODIWEF]
 functions as having no body.  which means strings are still there. I'm hving
 trouble removing the source entirely.
* TODO change the strip stuff to use constexpr - which from what ive been
     reading also doesnt strip it from the source entirely.
* TODO Buffer log entries if there are no registered loggers, use a flush
 command to release any buffered logs
* TODO optionally use streams for writing so that we dont have buffers sitting
     around the place
* TODO Fix warnings, its super annoying and really pollutes the output logs.

